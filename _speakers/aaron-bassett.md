---
name: Aaron Bassett
talks:
- "Can you keep a secret?"
---
Aaron Bassett has lived in Ireland, Scotland, and is currently wandering
around central and eastern Europe. He is a recovering senior software
engineer turned [award-winning Developer
Advocate](https://www.nexmo.com/blog/2018/11/28/all-your-devrel-award-
belong-to-us-dr/) at [Nexmo](https://nexmo.com) where he leads their content
creation.

Aaron has been working online since 2005 and has always enjoyed sharing what
he learned by organising and speaking at local meetups. He spoke at his
first conference in 2013, and since then he's spoken at conferences on a
range of topics all over the world. He has a passion for mentoring and has
been involved with [Social Innovation Camp
UK](https://en.wikipedia.org/wiki/Social_Innovation_Camp), [Social
Innovation Camp Kosovo](http://unicefstories.org/tag/social-innovation-camp-
kosovo/), [Startup Weekend](https://startupweekend.org/), [Open
Glasgow](http://futurecity.glasgow.gov.uk/hacking-the-future/),
[DjangoGirls](https://djangogirls.org/) and [global diversity CFP
day](https://www.globaldiversitycfpday.com/events/101)
