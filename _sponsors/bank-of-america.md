---
name: Bank of America
tier:
  - gold
  - social event
site_url: https://careers.bankofamerica.com
logo: bank-of-america.jpg
twitter: BankofAmerica
---
Quartz is a massive Python development initiative that is revolutionizing the Global Markets
business at Bank of America Merrill Lynch. Quartz will be the primary platform for pricing trades,
managing positions, and computing risk exposure. Our technology stack includes globally distributed
object-oriented databases, Linux compute farms, and advanced quant libraries. Thousands of
developers are using the highly-agile platform to deliver applications to thousands of end users.

We’re committed to helping make financial lives better by connecting our customers and clients to
the financial solutions they need and to helping the communities we serve.

[Find out more and apply today!](https://careers.bankofamerica.com/)
