---
duration: 25
presentation_url:
room:
slot:
speakers:
- Keenan Szulik
title: "Open source sustainability: what does this mean for Python?"
type: talk
video_url:
---
Across open source communities—from Python to JavaScript to Ruby—businesses
and developers alike are wondering how we can better sustain the critical
infrastructure that open source projects have become. This talk will survey
different approaches within the Python community (including foundations,
startups, individual maintainer efforts), why they're different, and how/why
you should get involved!
