---
duration: 25
presentation_url:
room:
slot:
speakers:
- Andrew Hao
title: "For Cryin\u0027 Out Loud! How Neural Networks Made Me a Better Parent"
type: talk
video_url:
---
Parenthood is hard! In the months after my son arrived, I found myself
struggling through depression, especially so during long episodes of infant
crying. So I coped by beginning a journey to build an AI baby monitor,
training a TensorFlow model to recognize and quantify my little one's cries.
But this process was fraught with confusion - I had never worked with these
models before. How do they work? And more importantly, how will knowing all
this help me survive the travails of early parenthood?

Join me on a journey through the inner workings of a TensorFlow audio
recognition model. Together, we'll see how it works by walking through a CNN
described from a Google research paper. We'll explore core TensorFlow model
concepts of weights, biases, and convolutions and learn how they work in the
process. Even if you don't have any experience in machine learning, you'll
walk away with key understanding of how ML models are structured so you can
apply them to your next project!
